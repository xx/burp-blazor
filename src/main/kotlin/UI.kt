package burp

import burp.api.montoya.MontoyaApi
import burp.api.montoya.core.ByteArray
import net.miginfocom.swing.MigLayout
import java.time.LocalDateTime
import javax.swing.*
import javax.swing.table.DefaultTableModel

const val TAB_NAME = "Blazor"

class UI(api: MontoyaApi) : JPanel() {
    private val api: MontoyaApi
    private var msgCount = 0
    private val messageStore = mutableListOf<Message>()

    // ui components
    private val mainSplitPane = JSplitPane()
    private val messageViewSplitPane = JSplitPane()
    private val messagesListPane = JScrollPane()
    private val messagesList = JTable()
    private val messageJsonView = api.userInterface().createRawEditor()
    private val messageResponseView = api.userInterface().createRawEditor()

    init {
        this.api = api

        initComponents()
        api.userInterface().registerSuiteTab(TAB_NAME, this)
    }

    private fun initComponents() {
        layout = MigLayout("fill,hidemode 3,align left top", "fill")

        messagesList.model = object : DefaultTableModel(
            arrayOf(), arrayOf(
                "#", "Connection ID", "Direction", "Type", "Arguments", "Date"
            )
        ) {
            override fun isCellEditable(row: Int, column: Int): Boolean {
                return false
            }

            override fun getColumnClass(columnIndex: Int): Class<*> {
                return when (columnIndex) {
                    0, 1 -> Int::class.javaObjectType
                    else -> String::class.javaObjectType
                }
            }
        }
        messagesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
        messagesList.autoCreateRowSorter = true
        messagesList.selectionModel.addListSelectionListener { tableSelected() }
        messagesList.autoResizeMode = JTable.AUTO_RESIZE_OFF

        messagesListPane.setViewportView(messagesList)

        listOf(80, 80, 160, 200, 740, 250).forEachIndexed { i, width ->
            messagesList.columnModel.getColumn(i).preferredWidth = width
        }

        messageViewSplitPane.orientation = JSplitPane.HORIZONTAL_SPLIT
        messageViewSplitPane.leftComponent = messageJsonView.uiComponent()
        messageViewSplitPane.rightComponent = messageResponseView.uiComponent()

        messageJsonView.setEditable(false)
        messageResponseView.setEditable(false)

        mainSplitPane.orientation = JSplitPane.VERTICAL_SPLIT
        mainSplitPane.topComponent = messagesListPane
        mainSplitPane.bottomComponent = messageViewSplitPane

        // finally add everything to the root component
        add(mainSplitPane, "push, grow")
    }

    private fun tableSelected() {
        if (messagesList.selectedRow == -1) return
        val selected =
            messagesList.model.getValueAt(messagesList.convertRowIndexToModel(messagesList.selectedRow), 0) as Int
        messageJsonView.contents = ByteArray.byteArray(messageStore[selected].raw)
        val response = findResponse(messageStore, messageStore[selected])
        messageResponseView.contents = ByteArray.byteArray(response?.raw ?: "")
    }

    fun appendMessages(id: Int, direction: String, messages: List<Message>) {
        val dtm = messagesList.model as DefaultTableModel
        messages.forEach {
            messageStore.add(it)
            dtm.addRow(
                arrayOf(
                    msgCount, // #
                    id, // Connection ID
                    direction, // Direction
                    it.javaClass.simpleName, // Type
                    when (it) { // Arguments
                        is InvocationMessage -> "${it.method}: ${it.arguments}"
                        is CompletionMessage -> it.extra
                        is UnknownMessage -> it.extra
                        else -> ""
                    }, LocalDateTime.now() // Date
                )
            )

            msgCount++
        }
    }
}