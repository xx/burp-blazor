package burp

fun findResponse(messages: List<Message>, source: Message): Message? {
    if (source !is InvocationMessage) {
        return null
    }

    val method = when (source.method) {
        "JS.BeginInvokeJS" -> "EndInvokeJSFromDotNet"
        "EndInvokeJSFromDotNet" -> "JS.BeginInvokeJS"
        "JS.RenderBatch" -> "OnRenderCompleted"
        "OnRenderCompleted" -> "JS.RenderBatch"
        else -> return null
    }

    val match = messages.filter {
        it is InvocationMessage && it.method == method && it.arguments[0] == source.arguments[0]
    }

    return when (match.size) {
        0 -> null
        1 -> match[0]
        else -> {
            log.debug("Found more than one match for $source?")
            null
        }
    }
}